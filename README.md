# リモートPCでROSノードを起動する
ネットワーク経由で接続された他のPC（リモートPC）でROSノードを起動させる。

通常はロボット（HSRやWhill）にキーボードやマウスを接続していないのでロボット内蔵のPC（リモートPC）での操作（ROSノードの立ち上げ）はSSHで接続して行っています。
しかし、手間ががかかるのでホストPCからリモート先でROSノードを起動する方法を試します。

本ページの説明は便宜上HSRCを使う場合を想定しています。他のロボット等を使う場合は適宜情報の修正をお願いします。

## To Do
2023/04/29<br>
launchファイルに hsr-userのパスワードを書いています．<br>
気持ち悪いので，　ｓｓｈの設定を変更してパスワード認証から鍵認証に変更したいけどできない・・・
```
Unable to establish ssh connection to [hsr-user@hsrc.local:22]: not a valid RSA private key file
```


## ネットワークの構成と事前の確認
- ロボット（HSRC） : hsr-user@hsrc.local

- ノートPC　: roboworks@Flow-Z13.local  (※みなさんの環境に置き換えて下さい）

### SSHによる接続確認
下記のようにノートPCからロボットにssh で接続できることを確認します．
```
roboworks@Flow-Z13:~$ ssh hsr-user@hsrc.local
Warning: Permanently added the ECDSA host key for IP address '192.168.11.8' to the list of known hosts.
hsr-user@hsrc.local's password:
Welcome to HSRC 21.12.01 focal ja customized using Cubic on 2021-12-13 14:13 (GNU/Linux 5.4.44-rt27-hsr-tmc x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 * Strictly confined Kubernetes makes edge and IoT secure. Learn how MicroK8s
   just raised the bar for easy, resilient and secure K8s cluster deployment.

   https://ubuntu.com/engage/secure-kubernetes-at-the-edge

355 updates can be installed immediately.
85 of these updates are security updates.
To see these additional updates run: apt list --upgradable

Last login: Sat Apr 29 17:02:53 2023 from fe80::457:2387:145b:f8df%enp0s31f6
hsr-user@hsrc:~$
```
上田センセのブログ　[【ROS】一つのlaunchファイルで複数のロボット・PCを動かす](https://b.ueda.tech/?post=20190706_ros)では

> どうやら~/.ssh/known_hostsに記録があるとダメなことがあるようで、
> ロボットのIPアドレスやホスト名に関する記録を全部消した上で、
>
> ssh ubuntu@raspimouse -oHostKeyAlgorithms='ssh-rsa' 

> と手でログインして、RSAアルゴリズムを使うように指定なおさないといけないとのことです。
とのことでしたが，私はHSRの初期設定のままで動きました．


## 公開鍵認証の設定
launchファイルに hsr-userのパスワードを書くのが気持ち悪い方は，　ｓｓｈの設定を変更してパスワード認証から鍵認証に変更しておくのがいいかもしれません．

```
$ ssh-copy-id -i ~/.ssh/id_rsa.pub <user name>@<ip address>
```

### HSRにROS開発環境をインストールする
そもそも，HSR側のROSノードを起動するので・・・

まだやってない方は，下記のように HSRにROS開発環境をインストールして下さい
```
ssh administrator@hsrc.local
sudo apt-get update
sudo apt-get install ros-noetic-hsrb-full

ssh hsr-user@hsrc.local
source /opt/ros/noetic/setup.bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace
cd ~/catkin_ws
catkin_make
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```


## ロボット（hsrc.local）側での設定
~/catkin_ws/devel/env.shに下記を追加して下さい．
```
export ROS_IP=`hostname -I | cut -d' ' -f1`
```

## ノートPC側の設定
ROS(noetic), HSR開発環境がインストールされていることを確認して下さい．

~/.bashrcに下記を追加して下さい．
```
export ROSLAUNCH_SSH_UNKNOWN=1
```


## ノートPC側の launch ファイルの例
[ros_tutorials](https://github.com/ros/ros_tutorials)にある，
talker.py　 をリモートPC（HSR），　listener.pyをノートPCで実行する　launchファイルは下記の通りです．

```
<launch>
<machine name="hsrc" address="hsrc.local" env-loader="/home/hsr-user/catkin_ws/devel/env.sh" user="hsr-user" password="******"/>

  <node  name="listener" pkg="rospy_tutorials" type="listener.py" output="screen"/>
  <node  machine="hsrc" name="talker" pkg="rospy_tutorials" type="talker.py" output="screen"/>
</launch>
```

下記のように， HSR側でPublishしたトピックをノートPC側でSubcribeしてます．
```
<hsrb>~$ roslaunch talker_listener_remote.launch
... logging to /home/roboworks/.ros/log/20230429-112514_82bfd488-e680-11ed-898e-0013953130f8/roslaunch-Flow-Z13-53713.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://192.168.11.10:40887/
remote[hsrc.local-0] starting roslaunch
remote[hsrc.local-0]: creating ssh connection to hsrc.local:22, user[hsr-user]
launching remote roslaunch child with command: [env ROS_MASTER_URI=http://hsrc.local:11311 /home/hsr-user/catkin_ws/devel/env.sh roslaunch -c hsrc.local-0 -u http://192.168.11.10:40887/ --run_id 20230429-112514_82bfd488-e680-11ed-898e-0013953130f8 --sigint-timeout 15.0 --sigterm-timeout 2.0]
remote[hsrc.local-0]: ssh connection created

SUMMARY
========

PARAMETERS
 * /rosdistro: noetic
 * /rosversion: 1.16.0

MACHINES
 * hsrc

NODES
  /
    listener (rospy_tutorials/listener.py)
    talker (rospy_tutorials/talker.py)

ROS_MASTER_URI=http://hsrc.local:11311

process[listener-1]: started with pid [53744]
[hsrc.local-0]: launching nodes...
[hsrc.local-0]: ROS_MASTER_URI=http://hsrc.local:11311
[hsrc.local-0]: process[talker-1]: started with pid [35281]
[hsrc.local-0]: ... done launching nodes
[INFO] [1682768507.552088]: /listenerI heard hello world 1682768507.5440834
[INFO] [1682768507.651184]: /listenerI heard hello world 1682768507.6440408
[INFO] [1682768507.751370]: /listenerI heard hello world 1682768507.7439146
[INFO] [1682768507.853207]: /listenerI heard hello world 1682768507.843836
[INFO] [1682768507.951200]: /listenerI heard hello world 1682768507.9438994
[INFO] [1682768508.053176]: /listenerI heard hello world 1682768508.0438735
[INFO] [1682768508.155140]: /listenerI heard hello world 1682768508.143811
[INFO] [1682768508.258743]: /listenerI heard hello world 1682768508.2438374
[INFO] [1682768508.350736]: /listenerI heard hello world 1682768508.3439007
...
...
```

# 実践
- HSRのマイクで音声をキャプチャーする<br>
audio_captureノードを利用

- 背中のPCのスピーカーから音声を再生する<br>
audio_playノードを利用

```
<launch>
  <machine name="hsrc" address="hsrc.local" env-loader="/home/hsr-user/catkin_ws/devel/env.sh" user="hsr-user" password="hsruser"/>


  <arg name="dst" default="appsink"/>
  <arg name="device" default=""/>
  <arg name="format" default="mp3"/>
  <arg name="bitrate" default="128"/>
  <arg name="channels" default="1"/>
  <arg name="depth" default="16"/>
  <arg name="sample_rate" default="16000"/>
  <arg name="sample_format" default="S16LE"/>
  <arg name="ns" default="audio"/>
  <arg name="audio_topic" default="audio"/>
  <arg name="do_timestamp" default="false"/>

  <group ns="$(arg ns)">
    <node name="audio_play" pkg="audio_play" type="audio_play" >
      <remap from="audio" to="$(arg audio_topic)" />
      <param name="dst" value="$(arg dst)"/>
      <param name="device" value="$(arg device)"/>
      <param name="do_timestamp" value="$(arg do_timestamp)"/>
      <param name="format" value="$(arg format)"/>
      <param name="channels" value="$(arg channels)"/>
      <param name="depth" value="$(arg depth)"/>
      <param name="sample_rate" value="$(arg sample_rate)"/>
      <param name="sample_format" value="$(arg sample_format)"/>
    </node>


  <node machine="hsrc" name="audio_capture" pkg="audio_capture" type="audio_capture" >
      <remap from="audio" to="$(arg audio_topic)" />
      <param name="dst" value="$(arg dst)"/>
      <param name="device" value="$(arg device)"/>
      <param name="format" value="$(arg format)"/>
      <param name="bitrate" value="$(arg bitrate)"/>
      <param name="channels" value="$(arg channels)"/>
      <param name="depth" value="$(arg depth)"/>
      <param name="sample_rate" value="$(arg sample_rate)"/>
      <param name="sample_format" value="$(arg sample_format)"/>
    </node>
  </group>
</launch>
```




# 参考にしたサイト

https://qiita.com/srs/items/309a16ae331da563c2e3

リモートでロボットの電源を切る
https://qiita.com/srs/items/a41c370268f1a19bf268

https://cyberworks.cocolog-nifty.com/blog/2013/09/roslaunchde-975.html

一つのlaunchファイルで複数のロボット・PCを動かす
https://b.ueda.tech/?post=20190706_ros


https://answers.ros.org/question/397506/remote-roslaunch/

https://github.com/jsk-ros-pkg/jsk_robot/issues/1322
